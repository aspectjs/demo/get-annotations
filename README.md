# AspectJS-demo-getannotations

This project is a simple showcase of the `getAnnotation` api of the[@aspectjs/core](https://www.npmjs.com/package/@aspectjs/core) package. It shows how to programatically retrieve the annotations across the codebase.
