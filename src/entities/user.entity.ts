import { Column, Entity, OneToMany } from "../annotations";
import { Post } from "./post.entity";

@Entity()
export class User {
  @Column()
  title: string;

  @Column()
  body: string;

  @Column()
  @OneToMany()
  posts: Post[];
}
