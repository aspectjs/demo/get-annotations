import { BeforePersist, Column, Entity } from "../annotations";

@Entity()
export class Post {
  @Column()
  title: string;

  @Column()
  body: string;

  @Column()
  publishDate: Date;

  @BeforePersist()
  setPublishDate() {
    this.publishDate = new Date();
  }
}
