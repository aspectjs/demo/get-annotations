import { AnnotationType } from "@aspectjs/common";
import { ANNOTATION_FACTORY } from "./annotation-factory";

export const BeforePersist = ANNOTATION_FACTORY.create(
  AnnotationType.METHOD,
  function BeforePersist() {}
);
