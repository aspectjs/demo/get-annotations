import { AnnotationType } from "@aspectjs/common";
import { ANNOTATION_FACTORY } from "./annotation-factory";

export const OneToMany = ANNOTATION_FACTORY.create(
  AnnotationType.PROPERTY,
  function OneToMany() {}
);
