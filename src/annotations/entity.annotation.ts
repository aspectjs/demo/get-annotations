import { AnnotationType } from "@aspectjs/common";
import { ANNOTATION_FACTORY } from "./annotation-factory";

export const Entity = ANNOTATION_FACTORY.create(
  AnnotationType.CLASS,
  function Entity() {}
);
