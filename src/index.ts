import { getAnnotations } from "@aspectjs/common";

import { OneToMany } from "./annotations";
import "./entities/post.entity";
import { Post } from "./entities/post.entity";
import "./entities/user.entity";
import { User } from "./entities/user.entity";

function main() {
  console.log(`All annotations found : ${getAnnotations().all().find()}`);
  console.log(
    `All annotations found on classes: ${getAnnotations().onClass().find()}`
  );
  console.log(
    `All annotations found on the User class: ${getAnnotations()
      .onClass(User)
      .find()}`
  );

  console.log(
    `All annotations found on methods of the Post class: ${getAnnotations()
      .onMethod(Post)
      .find()}`
  );

  console.log(
    `OneToMany() annotations found: ${getAnnotations(OneToMany).all().find()}`
  );
}

main();
